/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import org.apache.log4j.Logger;

public class log4j_example {

    private static Logger logger = Logger.getLogger(log4j_example.class);

    public static void main(String[] args) {

        if (logger.isDebugEnabled()) {
            logger.debug("This is debug");
        }

        if (logger.isInfoEnabled()) {
            logger.info("This is info");
        }

        logger.warn("This is warn");
        logger.error("This is error");
        logger.fatal("This is fatal");
        
//    TRACE   : Very detailed information. Should be written to logs only. Used only to track the program's flow at checkpoints.
//    DEBUG   : Detailed information. Should be written to logs only.
//    INFO    : Notable runtime events. Should be immediately visible on a console, so use sparingly.
//    WARNING : Runtime oddities and recoverable errors.
//    ERROR   : Other runtime errors or unexpected conditions.
//    FATAL   : Severe errors causing premature termination.

    }

}
